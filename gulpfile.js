var gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    header = require('gulp-header'),
    pkg = require('./package.json'),
    minifyCSS = require('gulp-minify-css'),
    watch = require('gulp-watch'),
    sass = require('gulp-sass'),
    imagemin = require('gulp-imagemin');



const imgFiles = '_assets/images/**/*';
    
gulp.task('scripts', function() {
    gulp.src([
        'bower_components/jquery/dist/jquery.js', // if you need jquery, use "npm i -g bower" and "bower install jquery"
        'bower_components/popper.js/dist/umd/popper.js', // if you need bootstrap, use "npm i -g bower" and "bower install bootstrap"
        'bower_components/bootstrap/dist/js/bootstrap.js', // if you need bootstrap, use "npm i -g bower" and "bower install bootstrap"
        'bower_components/fullpage.js/dist/jquery.fullpage.js', // if you need bootstrap, use "npm i -g bower" and "bower install bootstrap"
        'bower_components/prism/prism.js',
        '_assets/js/**/*.js' // our js files
    ])
        .pipe(concat('main.js')) // cancatenation to file main.js
        .pipe(uglify()) // uglifying this file
        .pipe(rename({suffix: '.min'})) // renaming file to myproject.min.js
        .pipe(header('/*! <%= pkg.name %> <%= pkg.version %> */\n', {pkg: pkg} )) // banner with version and name of package
        .pipe(gulp.dest('assets/js/')) // save file to destination directory
});

gulp.task('imgmin', () => {
  gulp.src(imgFiles)
    .pipe(imagemin({
        interlaced: true,
        progressive: true,
        optimizationLevel: 1,
        svgoPlugins: [{removeViewBox: true}],
        verbose: true
    })).pipe(gulp.dest('assets/images/'));
});

gulp.task('styles', function() {
    gulp.src([
        'bower_components/bootstrap/dist/css/bootstrap.css', // example with installed bootstrap package
        'bower_components/bootstrap/dist/css/bootstrap-theme.css', // example with installed bootstrap package
        'bower_components/fullpage.js/dist/jquery.fullpage.css',
        'bower_components/prism/themes/prism.css',
        'bower_components/prism/themes/prism-okaidia.css',
        '_assets/css/**/*.scss' // our styles
    ])
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('main.css')) // concatenation to file main.css
        .pipe(minifyCSS({keepBreaks:false})) // minifying file
        .pipe(rename({suffix: '.min'})) // renaming file to main.min.css
        .pipe(header('/*! <%= pkg.name %> <%= pkg.version %> */\n', {pkg: pkg} )) // making banner with version and name of package
        .pipe(gulp.dest('assets/css/')) // saving file main.min.css to this directory
});

gulp.task('watcher', function() {
    gulp.watch('_assets/js/**/*.js', ['scripts']);
    gulp.watch('_assets/sass/**/*.sass', ['styles']);
    gulp.watch('_assets/images/**/*', ['imgmin']);
});

gulp.task('default', ['scripts', 'styles', 'imgmin']); // start default tasks "gulp"
gulp.task('watch', ['watcher']); // start watcher task "gulp watch"