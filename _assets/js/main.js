$(document).ready(function() {
    // get current URL path and assign 'active' class
    var pathname = window.location.pathname;
    $('.nav > li > a[href="'+pathname+'"]').parent().find('a').addClass('active');

    $('#myContainer').fullpage({
        slidesNavigation: false,
        slidesToSections: false,
        responsiveWidth: 900,
        responsiveSlides: true,
        autoScrolling: true,
        keyboardScrolling: true,
        animateAnchor: true,
        recordHistory: true,
        lazyLoading: true,
        dragAndMove: true,
    });

    $('#toSections').click(function(){
        $.fn.fullpage.responsiveSlides.toSections();
    });

    $('#toSlides').click(function(){
        $.fn.fullpage.responsiveSlides.toSlides();
    });

    $(".loader").fadeOut();
})